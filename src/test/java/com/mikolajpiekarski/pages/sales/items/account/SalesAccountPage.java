package com.mikolajpiekarski.pages.sales.items.account;

import com.mikolajpiekarski.pages.base.PageElement;
import com.mikolajpiekarski.pages.sales.base.SalesBasePage;
import com.mikolajpiekarski.pages.sales.items.commonelements.DetailsCard;
import com.mikolajpiekarski.pages.sales.items.commonelements.modalitemoperation.ModalCreate;
import com.mikolajpiekarski.pages.sales.items.commonelements.modalitemoperation.ModalEdit;
import com.mikolajpiekarski.pages.sales.items.contact.SalesContactPage;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;

/**
 * Class representing an account specific page
 */
public class SalesAccountPage extends SalesBasePage {
    private final String accountName;

    public SalesAccountPage(WebDriver driver, String accountName) {
        super(driver, accountName + " | Salesforce");
        this.accountName = accountName;
    }

    // Locators
    private final By buttonEditLocator = By.xpath(
            "//div[contains(@class, 'oneContent active')]//button[@name='Edit']");
    private final By detailsTabLocator = By.xpath(
            "//div[contains(@class, 'oneContent active')]//li[@title='Details']/a");
    private final By relatedTabLocator = By.xpath(
            "//div[contains(@class, 'oneContent active')]//li[@title='Related']/a");

    // Navigation methods
    /**
     * Clicks the button to edit account details
     * @return Modal with details to edit
     */
    public ModalEdit clickButtonEdit(){
        findElement(buttonEditLocator).click();
        return new ModalEdit(getDriver(), accountName);
    }

    /**
     * Clicks the account details tab
     * @return Card containing the account details
     */
    public DetailsCard clickDetailsTab(){
        findElement(detailsTabLocator).click();
        return new DetailsCard(getDriver());
    }

    /**
     * Clicks the related tab
     * @return Card containing items related to the account
     */
    public AccountRelatedCard clickRelatedTab(){
        findElement(relatedTabLocator).click();
        return new AccountRelatedCard(getDriver());
    }

    /**
     * Class representing the related card/tab on the account page
     */
    public class AccountRelatedCard extends PageElement{
        public AccountRelatedCard(WebDriver driver) {
            super(driver.findElement(selfLocator), driver);
        }

        // Locators
        private static final By selfLocator = By.xpath("//div[contains(@class, 'oneContent active')]" +
                "//*[@aria-labelledby='relatedListsTab__item']");
        private final By buttonNewContactLocator = By.xpath(
                ".//li[contains(@data-target-selection-name, 'NewContact')]");

        // Dynamic locators
        private By getRelatedContactLocator(String contactName){
            return By.xpath(".//div[@class='listWrapper'][.//span[text()='Contacts']]" +
                    "//span[text()='" + contactName + "']");
        }

        // Navigation methods
        /**
         * Clicks the button to create new contact
         * @return Modal window with contact details to be filled
         */
        public ModalCreate clickNewContactButton(){
            findElement(buttonNewContactLocator).click();
            return new ModalCreate(getDriver(), "Contact");
        }

        /**
         * Clicks the related contact
         * @param contactName Name of the contact to be clicked
         * @return Page of the specified contact
         */
        public SalesContactPage clickRelatedContact(String contactName){
            try{
                findElement(getRelatedContactLocator(contactName)).click();
            } catch (NotFoundException e){
                throw new RuntimeException("Related contact not found: " + contactName);
            }

            return new SalesContactPage(getDriver(), contactName, accountName);
        }
    }
}

package com.mikolajpiekarski.pages.sales.items.commonelements.modalitemoperation;


import com.mikolajpiekarski.pages.sales.items.commonelements.modalitemoperation.base.ModalItemOperation;
import org.openqa.selenium.WebDriver;

/**
 * Class representing modal window displayed during creation or editing of an item
 */
public class ModalCreate extends ModalItemOperation {
    /**
     * Creates a ModalWindow object
     * @param driver Initalized driver
     * @param expectedItemType Expected type of the created item
     */
    public ModalCreate(WebDriver driver, String expectedItemType) {
        super(driver, "New", expectedItemType);
    }
}

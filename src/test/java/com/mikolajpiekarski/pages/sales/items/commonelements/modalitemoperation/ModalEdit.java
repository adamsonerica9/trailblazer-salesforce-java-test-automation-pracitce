package com.mikolajpiekarski.pages.sales.items.commonelements.modalitemoperation;

import com.mikolajpiekarski.pages.sales.items.commonelements.modalitemoperation.base.ModalItemOperation;
import org.openqa.selenium.WebDriver;

public class ModalEdit extends ModalItemOperation {
    /**
     * Creates a modal edit window object
     * @param driver Initialized driver
     * @param expectedItemName Expected name of the edited item
     */
    public ModalEdit(WebDriver driver, String expectedItemName) {
        super(driver, "Edit", expectedItemName);
    }
}

package com.mikolajpiekarski.pages.sales;

import com.mikolajpiekarski.pages.sales.base.SalesBasePage;
import org.openqa.selenium.WebDriver;

public class SalesHomePage extends SalesBasePage {
    public SalesHomePage(WebDriver driver) {
        super(driver, "Home | Salesforce");
    }
}

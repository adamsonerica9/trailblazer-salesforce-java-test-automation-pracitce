package com.mikolajpiekarski.pages.sales.base;

import com.mikolajpiekarski.pages.base.BasePage;
import com.mikolajpiekarski.pages.sales.base.commonelements.Header;
import com.mikolajpiekarski.pages.sales.base.commonelements.SalesNavbar;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

/**
 * Abstract class for sales app pages
 */
public abstract class SalesBasePage extends BasePage {
    private final Header header = new Header(getDriver());
    public Header getHeader() {
        return header;
    }

    private final SalesNavbar navbar = new SalesNavbar(getDriver());
    public SalesNavbar getNavbar() {
        return navbar;
    }

    // Constructors
    public SalesBasePage(WebDriver driver, String expectedPageTitle, Boolean titleContains, Duration timeout) {
        super(driver, expectedPageTitle, titleContains, timeout);
    }

    public SalesBasePage(WebDriver driver, String expectedPageTitle, Boolean titleContains) {
        this(driver, expectedPageTitle, titleContains, Duration.ofSeconds(10));
    }

    public SalesBasePage(WebDriver driver, String expectedPageTitle) {
        this(driver, expectedPageTitle, false, Duration.ofSeconds(10));
    }

    // Locators
    private final By alertMessageLocator = By.cssSelector(".toastMessage");

    // Navigation methods

    /**
     * Gets a message displayed in alert pop up
     * @return String containing message of the alert
     */
    public String getAlertMessage(){
        return findElement(alertMessageLocator).getText();
    }
}

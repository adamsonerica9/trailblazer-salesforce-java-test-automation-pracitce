package com.mikolajpiekarski.pages.sales.base.commonelements;

import com.mikolajpiekarski.pages.base.PageElement;
import com.mikolajpiekarski.pages.sales.items.account.SalesAccountListPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Class representing a sales navbar
 */
public class SalesNavbar extends PageElement {

    public SalesNavbar(WebDriver driver) {
        super(driver.findElement(selfLocator), driver);
    }

    // Locators
    private final static By selfLocator = By.cssSelector(".oneAppNavContainer");
    private final By tabAccountsLocator = By.xpath(".//a[@title='Accounts']/..");

    // Navigation methods

    /**
     * Clicks the 'Accounts' tab
     * @return Account list page object
     */
    public SalesAccountListPage clickTabAccounts(){
        findElement(tabAccountsLocator).click();

        return new SalesAccountListPage(getDriver());
    }

}

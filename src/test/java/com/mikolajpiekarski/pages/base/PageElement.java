package com.mikolajpiekarski.pages.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Abstract class representing subelements of pages
 */
public abstract class PageElement extends WebDriverUser {
    private final WebElement selfElement;
    public WebElement getSelf(){
        return selfElement;
    }

    /**
     * Sets up a page element
     * @param selfElement WebElement representing the element in the page
     * @param driver Initialized WebDriver
     */
    public PageElement(WebElement selfElement, WebDriver driver) {
        super(driver);
        this.selfElement = selfElement;
    }

    /**
     * Helper method for increased readability
     * @param by Locator
     * @return Found WebElement
     */
    @Override
    public WebElement findElement(By by) {
        return this.selfElement.findElement(by);
    }

    /**
     * Helper method for increased readability
     * @param by Locator
     * @return Found WebElement list
     */
    @Override
    public List<WebElement> findElements(By by) {
        return this.selfElement.findElements(by);
    }
}

package com.mikolajpiekarski.pages.base;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;

/**
 * BasePage abstract class for page objects to inherit
 */
public abstract class BasePage extends WebDriverUser {
    private final String pageTitle;
    public String getPageTitle() {
        return pageTitle;
    }

    // Constructor
    /**
     * Checks if expected and actual page titles match, if not then the wrong page is accessed
     * @param driver Running WebDriver to navigate the page
     * @param expectedPageTitle Expected title of the page
     * @param titleContains If true, expected title will be verified using contains instead of equals
     * @param timeout Title verification timeout, uses driver's implicitTimeoutDuration as default
     */
    public BasePage(WebDriver driver, String expectedPageTitle, Boolean titleContains, Duration timeout) {
        super(driver);

        try{
            if(titleContains)
                waitUntil(ExpectedConditions.titleContains(expectedPageTitle), timeout);
            else
                waitUntil(ExpectedConditions.titleIs(expectedPageTitle), timeout);
        }
        catch (TimeoutException e){
            throw new RuntimeException("Wrong page accessed: " +
                    "expected " + expectedPageTitle + ", actual " + driver.getTitle(), e);
        }

        this.pageTitle = driver.getTitle();
    }

    /**
     * See {@link #BasePage(WebDriver, String, Boolean, Duration)}
     */
    public BasePage(WebDriver driver, String expectedPageTitle, Boolean titleContains){
        this(driver, expectedPageTitle, titleContains, driver.manage().timeouts().getImplicitWaitTimeout());
    }

    /**
     * See {@link #BasePage(WebDriver, String, Boolean, Duration)}
     */
    public BasePage(WebDriver driver, String expectedPageTitle){
        this(driver, expectedPageTitle, false);
    }

}

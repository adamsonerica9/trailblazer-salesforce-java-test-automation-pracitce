package com.mikolajpiekarski.test.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mikolajpiekarski.pages.HomePage;
import com.mikolajpiekarski.pages.LogInPage;
import com.mikolajpiekarski.pages.sales.SalesHomePage;
import com.mikolajpiekarski.pages.sales.base.commonelements.Header;
import com.mikolajpiekarski.test.config.Credentials;
import com.mikolajpiekarski.test.config.DriverConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

public abstract class BaseTest {

    protected WebDriver driver;
    protected SalesHomePage salesHomePage;

    @BeforeClass
    public void beforeClass(){
        this.driver = setUpDriver();
        HomePage homePage = logIn();
        salesHomePage = goToSalesPage(homePage);
    }

    @AfterClass
    public void tearDown(){
        logOut();
        this.driver.quit();
    }

    private void logOut() {
        var header = new Header(driver);
        var userProfileDialog = header.clickUserProfile();
        userProfileDialog.clickLogout();
    }

    // Helper methods
    /**
     * Goes to Sales home page
     * @param homePage HomePage object to operate on
     */
    private SalesHomePage goToSalesPage(HomePage homePage) {
        homePage.clickAppNavigationButton();
        return homePage.clickNavigationSalesApp();
    }

    /**
     * Logs in with credentials specified in credentials.json
     * @return Home page object
     */
    private HomePage logIn() {
        LogInPage logInPage = new LogInPage(driver);
        Credentials credentials = readJsonFile("src/test/resources/test_config/credentials.json",
                Credentials.class);
        logInPage.fillUsername(credentials.getUsername());
        logInPage.fillPassword(credentials.getPassword());
        return logInPage.clickLogIn();
    }

    /**
     * Sets up the driver and loads the initial page
     * @return Configured driver
     */
    private WebDriver setUpDriver() {
        DriverConfig driverConfig = readJsonFile("src/test/resources/test_config/driver_config.json",
                DriverConfig.class);
        WebDriver driver = switch (driverConfig.getBrowser()) {
            case "chrome" -> new ChromeDriver();
            case "edge" -> new EdgeDriver();
            default -> throw new WebDriverException("Browser " + driverConfig.getBrowser() + " not supported");
        };
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(driverConfig.getImplicitWait()));

        driver.get(driverConfig.getStartUrl());
        driver.manage().window().maximize();

        return driver;
    }

    /**
     * Reads json file and converts it to the specified class/object
     * @param filePath Path to the json file
     * @param classToBeMappedInto Class for the json file to be mapped into
     * @return Object with mapped json contents
     * @param <T> Class of the returned object
     */
    protected <T> T readJsonFile(String filePath, Class<T> classToBeMappedInto) {
        ObjectMapper objectMapper = new ObjectMapper();
        T data;

        try {
            File file = new File(filePath);
            data = objectMapper.readValue(file, classToBeMappedInto);
        } catch (FileNotFoundException e){
            throw new RuntimeException("File not found: " + filePath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return data;
    }
}

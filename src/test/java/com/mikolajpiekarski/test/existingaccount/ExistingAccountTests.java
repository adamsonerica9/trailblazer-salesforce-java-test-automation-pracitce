package com.mikolajpiekarski.test.existingaccount;

import com.mikolajpiekarski.pages.sales.base.commonelements.SalesNavbar;
import com.mikolajpiekarski.pages.sales.items.account.SalesAccountListPage;
import com.mikolajpiekarski.test.base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.Map;


public class ExistingAccountTests extends BaseTest {
    private SalesAccountListPage salesAccountListPage;

    @BeforeMethod
    private void beforeMethod(){
        var navbar = new SalesNavbar(driver);
        salesAccountListPage = navbar.clickTabAccounts();
        salesAccountListPage.clickDropdownSelectedListView();
        salesAccountListPage.clickDropdownItemAllAccountsListView();
    }

    @Test (testName = "Edit existing account",
            dataProvider = "editExistingAccountDataProvider")
    public void editExistingAccount(String accountName, HashMap<String, String> data){
        // Go to the specified account page and click edit
        var salesAccountPage = salesAccountListPage.clickAccount(accountName);
        var editAccountModal = salesAccountPage.clickButtonEdit();

        // Input edit values into the modal window form and save
        editAccountModal.inputValuesToForm(data);
        editAccountModal.clickSaveButton();

        // Check if alert message confirms the change
        assertAlertMessageContains(salesAccountPage.getAlertMessage(), "was saved");

        // Compare input data with newly edited account details
        var details = salesAccountPage.clickDetailsTab();
        var output = details.getDetails();
        assertInputDataMatchesOutputData(data, output);
    }

    @Test (testName = "Create related contact",
            dataProvider = "createRelatedContactDataProvider")
    public void createRelatedContact(String accountName, HashMap<String, String> data){
        // Go to the specified account page and create new related contact
        var salesAccountPage = salesAccountListPage.clickAccount(accountName);
        var relatedCard = salesAccountPage.clickRelatedTab();
        var editAccountModal = relatedCard.clickNewContactButton();

        // Input new contact details into the modal window form and save
        editAccountModal.inputValuesToForm(data);
        editAccountModal.clickSaveButton();

        // Check if alert message confirms the creation
        assertAlertMessageContains(salesAccountPage.getAlertMessage(), "was created");

        // Go to newly created related contact page
        String contactName = data.get("first name") + " " + data.get("last name");
        var contactPage = relatedCard.clickRelatedContact(contactName);

        // Click details tab and check if newly created contact details match the input data
        var contactDetailsCard = contactPage.clickDetailsTab();
        var output = contactDetailsCard.getDetails();
        assertInputDataMatchesOutputData(data, output);

        // Check if the newly created contact can be found in the global search
        var header = contactPage.getHeader();
        var searchDialog = header.clickSearch();
        searchDialog.searchFor(contactName);
        boolean isContactFound = searchDialog.checkResult(contactName);
        searchDialog.exitSearchDialog();
        Assert.assertTrue(isContactFound,
                "Couldn't find " + contactName + " in global search");

        // Test cleanup - removes the newly created contact
        contactPage.clickMoreActionsArrowButton();
        contactPage.clickDeleteButton();
        salesAccountPage = contactPage.clickDeleteModalButton();
    }

    @DataProvider
    private Object[][] createRelatedContactDataProvider(){
        ExistingAccountParameters[] parameters = readJsonFile(
                "src/test/resources/test_data/existing_account/existing_account_tests.json",
                ExistingAccountParameters[].class);

        Object[][] objects = new Object[parameters.length][2];

        for (int i = 0; i < parameters.length; i++) {
            objects[i][0] = parameters[i].accountName;
            objects[i][1] = parameters[i].createRelatedContactParameters;
        }

        return objects;
    }

    @DataProvider
    private Object[][] editExistingAccountDataProvider(){
        ExistingAccountParameters[] parameters = readJsonFile(
                "src/test/resources/test_data/existing_account/existing_account_tests.json",
                ExistingAccountParameters[].class);

        Object[][] objects = new Object[parameters.length][2];

        for (int i = 0; i < parameters.length; i++) {
            objects[i][0] = parameters[i].accountName;
            objects[i][1] = parameters[i].editExistingAccountParameters;
        }

        return objects;
    }

    /**
     * Checks if alert message confirms the performed action
     * @param alertMessage Alert message
     * @param expectedPerformedAction Type of the expected action confirmed by the alert
     */
    private void assertAlertMessageContains(String alertMessage, String expectedPerformedAction) {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(alertMessage.contains(expectedPerformedAction),
                "Alert message '" + alertMessage + "' does not contain '" + expectedPerformedAction + "'");
    }

    /**
     * Checks if input data matches the output data
     * @param inputData Map with input data
     * @param outputData Map with details output data
     */
    private void assertInputDataMatchesOutputData(Map<String, String> inputData, Map<String, String> outputData){
        SoftAssert softAssert = new SoftAssert();
        
        for (Map.Entry<String, String> entry :
                inputData.entrySet()) {
            entry = formatEntry(entry);
            String actual = outputData.get(entry.getKey());
            String expected = entry.getValue();

            if (entry.getKey().contains("address")){
                // Addresses are asserted differently, by checking if input data address detail is in output data
                // full address
                softAssert.assertTrue(
                        actual.contains(expected), entry.getKey() + " does not contain: " + expected);
            }
            else{
                softAssert.assertEquals(actual, expected);
            }
        }
    }

    /**
     * Formats the input entry to match the data extracted from details tab
     * @param entry Map entry
     * @return Map entry with lowercase key and formatted value
     */
    private Map.Entry<String, String> formatEntry(Map.Entry<String, String> entry) {
        String keyLowercase = entry.getKey().toLowerCase();
        String value = entry.getValue();

        // --None-- dropbox options are displayed as empty
        if (entry.getValue().equals("--None--")){
            value = "";
        }
        else {
            // Addresses in the details tab are put in a single address field, so the key in input data needs to be
            // formatted to match the address field
            String[] addressDetails = new String[] {"street", "city", "state/province", "zip/postal code", "country"};
            for (String addressDetail :
                    addressDetails) {
                if (keyLowercase.contains(addressDetail)){
                    keyLowercase = getAddressTypeFromAddressDetail(keyLowercase);
                    break;
                }
            }
        }

        return Map.entry(keyLowercase, value);
    }

    /**
     * Gets the type of address from the address detail, for example turns 'billing street' into 'billing address'
     * @param addressDetail Address detail
     * @return Address type
     */
    private String getAddressTypeFromAddressDetail(String addressDetail) {
        String[] detailSplit = addressDetail.split(" ");
        String addressType;
        if (detailSplit.length > 1) {
            addressType = detailSplit[0] + " address";
        } else {
            addressType = "address";
        }

        return addressType;
    }
}

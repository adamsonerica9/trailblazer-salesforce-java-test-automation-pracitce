package com.mikolajpiekarski.test.existingaccount;

import java.util.Map;

public class ExistingAccountParameters {
    public String accountName;
    public Map<String, String> editExistingAccountParameters;
    public Map<String, String> createRelatedContactParameters;
}
